import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagelinksComponent } from './pagelinks.component';

describe('PagelinksComponent', () => {
  let component: PagelinksComponent;
  let fixture: ComponentFixture<PagelinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagelinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagelinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
