import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';
import { CreateimageComponent } from './pages/createimage/createimage.component';
import { PagelinksComponent } from './pages/pagelinks/pagelinks.component';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    CreateimageComponent,
    PagelinksComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
     HttpClientModule,
    AgGridModule.withComponents([CreateimageComponent,PagelinksComponent])
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
